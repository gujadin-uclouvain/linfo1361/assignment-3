# MAIN VARIABLES
LINK_SSH = "git@bitbucket.org:agerniers/fanorona_game.git"

clone: 
	cd 
	git clone $(LINK_SSH)
download: clone

update:
	pwd
	git -C ./archives/fanorona_game pull
pull: update