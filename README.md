[LINFO1361] IA - Assignment n°3
---

**Membres:**

* Guillaume Jadin
* Thomas Robert


**Description:**

* Implement a basic agent
* Implement a smart agent (3 parts)

**Get Code Template**

* Link: https://bitbucket.org/agerniers/fanorona_game

* `make clone` or `make download` to clone the git repository
* `make update` or `make pull` to pull modifications from the git repository

**INGInious:**

* https://inginious.info.ucl.ac.be/course/LINGI2261
