import random

from core.player import Color
from fanorona.fanorona_player import FanoronaPlayer
from fanorona.fanorona_rules import FanoronaRules
import copy


class AI(FanoronaPlayer):
    name = "Smart Player"

    def __init__(self, color):
        super(AI, self).__init__(self.name, color)
        self.position = color.value

    def play(self, state, remain_time):
        print("")
        print(f"Player {self.position} is playing.")
        print("time remain is ", remain_time, " seconds")
        res = minimax_search(state, self)
        # res = iterative_deepening_search(state, self)
        return res

    """
    The successors function must return (or yield) a list of
    pairs (a, s) in which a is the action played to reach the
    state s.
    """
    def successors(self, state):
        successors_states = []
        actions = FanoronaRules.get_player_actions(state, self.position)
        for action in actions:
            valid_action = FanoronaRules.act(copy.deepcopy(state), action, self.position)
            if valid_action is not False and type(valid_action) == tuple:
                successors_states.append((action, valid_action[0], self.evaluate(valid_action[0])))
        successors_states = sorted(successors_states, key=lambda i: -i[2])
        ret = []
        for i in successors_states:
            ret.append((i[0], i[1]))
        return ret

    """
    The cutoff function returns true if the alpha-beta/minimax
    search has to stop and false otherwise.
    """
    def cutoff(self, state, depth):
        return FanoronaRules.is_end_game(state) or depth > 5

    """
    The evaluate function must return an integer value
    representing the utility function of the board.
    """
    def evaluate(self, state):
        ours = state.get_board().get_player_pieces_on_board(Color.white if self.position == -1 else Color.green) #liste de nos pieces sur le board
        their = state.get_board().get_player_pieces_on_board(Color.green if self.position == -1 else Color.white) #Liste des pieces ennemy sur le board
        o_len, t_len = len(ours), len(their)
        if o_len == 0 or t_len == 0:
            nbr_pieces = state.score[self.position] - (1/2)*state.score[-self.position]
            player = 1 if state.get_next_player() == self.position else -1
            rwd_move = nbr_pieces * player if state.rewarding_move else 0

            return nbr_pieces + rwd_move

        else:
            nbr_pieces = state.score[self.position] - (1/2)*state.score[-self.position]
            player = 1 if state.get_next_player() == self.position else -1
            rwd_move = nbr_pieces * player if state.rewarding_move else 0

            distance = manhattan_distance(ours[round(0)], their[round(0)])  # 0 or use o_len/2 & t_len/2
            return nbr_pieces - distance + rwd_move


def manhattan_distance(a, b):
    """
    Basic formula of the Manhattan distance
    """
    return abs(a[1] - b[1]) #+ abs(a[0] - b[0])

# def iterative_deepening_search(state, player, prune=True):
#     action = None
#     next_state = copy.deepcopy(state)
#     for depth in range(0, 5):
#         new_action = minimax_search(next_state, player, prune)
#         if new_action is None: return action
#         action = new_action
#         next_state, is_done = FanoronaRules.act(next_state, action, player.position)
#         if is_done: return action
#     return action

"""
MiniMax and AlphaBeta algorithms.
Adapted from:
    Author: Cyrille Dejemeppe <cyrille.dejemeppe@uclouvain.be>
    Copyright (C) 2014, Universite catholique de Louvain
    GNU General Public License <http://www.gnu.org/licenses/>
"""

inf = float("inf")


def minimax_search(state, player, prune=True):
    """Perform a MiniMax/AlphaBeta search and return the best action.

    Arguments:
    state -- initial state
    player -- a concrete instance of class AI implementing an Alpha-Beta player
    prune -- whether to use AlphaBeta pruning

    """

    def max_value(state, alpha, beta, depth):
        if player.cutoff(state, depth):
            return player.evaluate(state), None
        val = -inf
        action = None
        for a, s in player.successors(state):
            if s.get_latest_player() == s.get_next_player():  # next turn is for the same player
                v, _ = max_value(s, alpha, beta, depth + 1)
            else:  # next turn is for the other one
                v, _ = min_value(s, alpha, beta, depth + 1)
            if v > val:
                val = v
                action = a
                if prune:
                    if v >= beta:
                        return v, a
                    alpha = max(alpha, v)
        return val, action

    def min_value(state, alpha, beta, depth):
        if player.cutoff(state, depth):
            return player.evaluate(state), None
        val = inf
        action = None
        for a, s in player.successors(state):
            if s.get_latest_player() == s.get_next_player():  # next turn is for the same player
                v, _ = min_value(s, alpha, beta, depth + 1)
            else:  # next turn is for the other one
                v, _ = max_value(s, alpha, beta, depth + 1)
            if v < val:
                val = v
                action = a
                if prune:
                    if v <= alpha:
                        return v, a
                    beta = min(beta, v)
        return val, action

    _, action = max_value(state, -inf, inf, 0)
    return action
